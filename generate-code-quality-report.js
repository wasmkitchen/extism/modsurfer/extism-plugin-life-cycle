//const fs = require('fs')
import { readFileSync, writeFileSync } from 'fs'

const args = process.argv.slice(2);
let moduleName = args[0]

const jsonString = readFileSync('./modsurfer-payload.json').toString();
const modsurferSmells = JSON.parse(jsonString);

function generateUniqueID() {
    let uniqueID = Math.random().toString(36).substr(2, 8);
    return uniqueID;
}

let getNewGitLabSmell = (description, severity, path) => {
    return {
        engine_name: "modsurfer",
        fingerprint: generateUniqueID(),
        description: description,
        severity: severity,
        location: {
            path: path,
            lines: {
                begin: 0
            }
        }
    }
}

let gitLabSmells = []

// info, minor, major, critical, or blocker
let getSeverity = severity => {
    if(severity < 5) return "info"
    if(severity < 10) return "minor"
    if(severity < 15) return "major"
    if(severity < 20) return "critical"
    return "blocker"
}

for (var smell in modsurferSmells["fails"]) {
    console.log("🤚", smell)
    let classification = modsurferSmells["fails"][smell].classification
    let severity = getSeverity(modsurferSmells["fails"][smell].severity)
    let actual = modsurferSmells["fails"][smell].actual
    let expected = modsurferSmells["fails"][smell].expected
    let description = `${smell}: classification: ${classification} | actual: ${actual} | expected: ${expected}`
    
    gitLabSmells.push(getNewGitLabSmell(
        description,severity,moduleName
    ))
    
}

writeFileSync('./gl-code-quality-report.json', JSON.stringify(gitLabSmells))

if(gitLabSmells.length>0) {
  console.log("🟥 ModSurfer:", gitLabSmells.length, "smell(s) detected 😡")
  process.exit(1)
} else {
  console.log("🟩 ModSurfer: everithing is fine 🤗")
  process.exit(0)
}
