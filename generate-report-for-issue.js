//const fs = require('fs')
import { readFileSync, writeFileSync } from 'fs'

const jsonString = readFileSync('./modsurfer-payload.json').toString()
const modsurferSmells = JSON.parse(jsonString)

/*
┌────────┬───────────────────────────────────┬────────────┬────────┬───────────────────┬────────────┐
│ Status │ Property                          │ Expected   │ Actual │ Classification    │ Severity   │
╞════════╪═══════════════════════════════════╪════════════╪════════╪═══════════════════╪════════════╡
│ FAIL   │ exports.include.helloWorld.params │ [I32, I32] │ []     │ ABI Compatibility │ ||||||||   │
├────────┼───────────────────────────────────┼────────────┼────────┼───────────────────┼────────────┤
│ FAIL   │ exports.max                       │ <= 2       │ 6      │ Security          │ |||||||||| │
└────────┴───────────────────────────────────┴────────────┴────────┴───────────────────┴────────────┘
*/

// info, minor, major, critical, or blocker
let getSeverity = severity => {
    if(severity < 5) return "🔵 info"
    if(severity < 10) return "🟡 minor"
    if(severity < 15) return "🟠 major"
    if(severity < 20) return "🔴 critical"
    return "⚫️ blocker"
}

let report = []
report.push("| Property | Expected | Actual | Classification | Severity |")
report.push("| -------- | -------- | ------ | -------------- | -------- |")

for (var smell in modsurferSmells["fails"]) {

    let classification = modsurferSmells["fails"][smell].classification
    let severity = getSeverity(modsurferSmells["fails"][smell].severity)
    let actual = modsurferSmells["fails"][smell].actual
    let expected = modsurferSmells["fails"][smell].expected

    let newLine = `| \`${smell}\` | \`${expected}\` | \`${actual}\` | ${classification} | ${severity} |`
    //let newLine = `| ${smell} | ${expected} | ${actual} | ${classification} | ${severity} |`

    report.push(newLine)
    
}

if (report.length > 2) {
    let mdReport = report.join("\n")
    writeFileSync('./gl-code-quality-report.md', mdReport)

    const url = `https://gitlab.com/api/v4/projects/${process.env.CI_PROJECT_ID}/issues`    
    const data = {
        title: "[ModSurfer] wasm vulnerability",
        labels: "wasm-vulnerability",
        description: mdReport
    }

    /*
        set `process.env.GITLAB_PAT_TOKEN_API` inside the CI variables
        https://gitlab.com/groups/wasmkitchen/extism/modsurfer/-/settings/ci_cd
    */
    const customHeaders = {
        "Content-Type": "application/json",
        "PRIVATE-TOKEN": process.env.GITLAB_PAT_TOKEN_API 
    }

    fetch(url, {
        method: "POST",
        headers: customHeaders,
        body: JSON.stringify(data),
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
    })

}
