# Extism Plugin Life-Cycle

## Container images

- registry.gitlab.com/wasmkitchen/extism/extism-tools/extism-golang

## Plugins registry

- https://gitlab.com/wasmkitchen/extism/extism-plugins-registry
- Project ID: 44818593

## Download the wasm plugin

```bash
echo "📦 ${WASM_PACKAGE}"
echo "📝 ${WASM_MODULE} ${WASM_VERSION}"

curl "https://gitlab.com/api/v4/projects/${GITLAB_WASM_PROJECT_ID}/packages/generic/${WASM_PACKAGE}/${WASM_VERSION}/${WASM_MODULE}" \
     --output ${WASM_MODULE}
```
