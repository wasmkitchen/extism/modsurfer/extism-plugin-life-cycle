// import or require (depending on your module system)
//import { ExtismContext } from "https://extism-ide.local:9090/proxy/8000/node_modules/@extism/runtime-browser/dist/index.esm.js"
import { ExtismContext } from "./extism.0.2.7.js"

const manifest = { wasm: [{ path: "hello-go.wasm" }] }

const ctx = new ExtismContext()
const plugin = await ctx.newPlugin(manifest)

// call the function 'count_vowels' defined in the wasm module
let output = await plugin.call("helloWorld", "Hey 👋 I'm Bob! 😀")
let jsonOutput = JSON.parse(new TextDecoder().decode(output))

console.log(jsonOutput)

document.querySelector("h1").innerText = jsonOutput.input
document.querySelector("h2").innerText = jsonOutput.message

// python3 -m http.server 8000