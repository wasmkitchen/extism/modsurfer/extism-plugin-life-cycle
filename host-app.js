import { Context, HostFunction, ValType } from '@extism/extism'
import { readFileSync } from 'fs'

const args = process.argv.slice(2);

let pathModuleName = args[0]
let functionName = args[1]
let param = args[2]

let wasm = readFileSync(pathModuleName)

// Create the WASM plugin
let ctx = new Context()
let plugin = ctx.plugin(wasm, true, [])

// Call the WASM function,
let buf = await plugin.call(functionName, param); 
let result = buf.toString()

console.log("From Node.js Host: ", result)
