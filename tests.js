import { Context, HostFunction, ValType } from '@extism/extism'
import { readFileSync, writeFileSync } from 'fs'

const args = process.argv.slice(2);

let pathModuleName = args[0]

let wasm = readFileSync(pathModuleName)


let failureTestCase = (testName, message) => 
`<testcase classname="test_case" name="${testName}">
	<failure message="${message}"/>
</testcase>`

let successTestCase = (testName) => 
`<testcase classname="test_case" name="${testName}">
</testcase>`

let testSuitesTemplate = (xmlTestCases, tests, failures) => {
	return `<?xml version="1.0" encoding="UTF-8"?>
<testsuites>
	<testsuite name="junit-report" tests="${tests}" failures="${failures}" errors="0" skipped="0">
	${xmlTestCases}
	</testsuite>
</testsuites>
`
}

var testCases = []
var tests = 0
var failures = 0

async function testCaseFunction(functionName, functionArg, testCase) {
	// Create the WASM plugin
	let ctx = new Context()
	let plugin = ctx.plugin(wasm, true, [])
	let buf = await plugin.call(functionName, functionArg)

	let result = buf.toString()
	let success = testCase(result)

	tests+=1

	if(success) {
		testCases.push(successTestCase(`Test of ${functionName}`))
	} else {
		testCases.push(failureTestCase(`Test of ${functionName}`, `😡 Call of ${functionName} | result: ${result}`))
		failures+=1
	}	
}


await testCaseFunction("helloWorld", "Hey 👋 I'm Bob! 😀", (result) => {
	console.log("🤖 Tests of helloWorld from Node.js Host: ", result)
	if(JSON.parse(result).input=="Hey 👋 I'm Bob! 😀") {
		return true
	} else {
		return false
	}
})

await testCaseFunction("hello", "", (result) => {
	console.log("🤖 Tests of hello from Node.js Host: ", result)
	if(result=="hello") {
		return true
	} else {
		return false
	}
})


writeFileSync('./test-results.xml', testSuitesTemplate(testCases.join("\n"), tests, failures))

